import boom from 'boom';
import { CacheService } from '../services/cache/cache.service';
import { calculateResult } from '../services/calculator.service';

export const postEvaluationFactory = (cache: CacheService) => async (formula: string): Promise<{ result: string }> => {
  try {
    const cachedResult = await cache.get<string>(formula);

    if (cachedResult) {
      return {
        result: cachedResult,
      };
    }
    const result = calculateResult(formula);
    await cache.set(formula, result);
    return { result };
  } catch (err) {
    throw boom.boomify(err);
  }
};
