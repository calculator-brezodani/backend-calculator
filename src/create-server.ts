import fastify, { FastifyInstance } from 'fastify';
import { CacheService } from './services/cache/cache.service';
import { routesFactory } from './routes/routes';
import fastifyCors from 'fastify-cors';

export const createServer = async ({ cacheService }: { cacheService: CacheService }): Promise<FastifyInstance> => {
  const server = fastify({ logger: true });
  await server.register(fastifyCors, {
    origin: 'http://localhost:3000',
  });
  await server.register(routesFactory({ cacheService }));
  return server;
};
