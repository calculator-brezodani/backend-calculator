export type CacheService = {
  get: <T>(key: string) => Promise<T | null>;
  set: <T>(key: string, val: T) => Promise<void>;
};
