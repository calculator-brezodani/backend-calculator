import { RedisClient } from 'redis';
import { CacheService } from './cache.service';

export const cacheServiceRedisFactory = ({ client, ttl }: { client?: RedisClient; ttl: number }): CacheService => {
  const get = async <T>(key: string): Promise<T | null> => {
    if (!client) {
      return null;
    }
    return new Promise((resolve, reject) => {
      client.get(key, (err, val) => {
        if (err) {
          return reject(err);
        }
        if (val === null) {
          return resolve(null);
        }
        const obj: T = JSON.parse(val);
        resolve(obj);
      });
    });
  };
  const set = async (key: string, val: unknown): Promise<void> => {
    if (!client) {
      return;
    }
    return new Promise((resolve, reject) => {
      const str = JSON.stringify(val);
      client.set(key, str, 'EX', ttl, (err, _) => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  };
  return { get, set };
};
