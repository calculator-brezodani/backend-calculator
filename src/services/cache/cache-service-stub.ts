import { CacheService } from './cache.service';

export const cacheServiceStub: CacheService = {
  get: async () => null,
  set: async () => {},
};
