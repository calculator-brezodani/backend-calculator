import calculator from 'exact-math';

export const calculateResult = (value: string): string => {
  const result = calculator.formula(value);
  return result;
};
