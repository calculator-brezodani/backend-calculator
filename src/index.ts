import { createClient } from 'redis';
import { createServer } from './create-server';
import { cacheServiceRedisFactory } from './services/cache/cache-service-redis';

const DAY_IN_SECONDS = 86400;

const thrower = (err: unknown): (() => void) => (): void => {
  throw err;
};

process.on('unhandledRejection', (err): void => {
  setTimeout(thrower(err), 0);
});

const main = async () => {
  const redisClient = createClient({ url: process.env.REDIS_URL || 'redis://0.0.0.0:6379' });
  const cacheService = cacheServiceRedisFactory({ client: redisClient, ttl: DAY_IN_SECONDS });
  const server = await createServer({ cacheService });
  return server.listen(9100, '0.0.0.0');
};

if (require.main) {
  main().catch((err): void => {
    setTimeout(thrower(err), 0);
  });
}
