import { FastifyInstance, FastifyError, FastifyRequest } from 'fastify';
import { CacheService } from '../services/cache/cache.service';
import * as resultsController from '../controllers/results-controller';

type CalculateRequest = { Body: { formula: string } };

export const routesFactory = ({ cacheService }: { cacheService: CacheService }) => (
  server: FastifyInstance,
  _: never,
  next: (err?: FastifyError) => void,
): void => {
  const postEvaluation = resultsController.postEvaluationFactory(cacheService);
  server.route({
    url: '/status',
    logLevel: 'warn',
    method: ['GET', 'HEAD'],
    handler: async (_, reply) => {
      return reply.send({ timeStamp: new Date() });
    },
  });
  server.route({
    url: '/calculate',
    logLevel: 'warn',
    method: ['POST', 'HEAD'],
    schema: {
      body: {
        type: 'object',
        additionalProperties: false,
        required: ['formula'],
        properties: {
          formula: {
            type: 'string',
          },
        },
      },
    },
    handler: (req: FastifyRequest<CalculateRequest>) => postEvaluation(req.body.formula),
  });
  next();
};
