import { FastifyInstance } from 'fastify';
import { createServer } from '../create-server';
import { cacheServiceStub } from '../services/cache/cache-service-stub';

describe('route tests', () => {
  let app: FastifyInstance;

  beforeAll(async () => {
    app = await createServer({
      cacheService: cacheServiceStub,
    });
    await app.ready();
  });

  it('should evaluate formula', async () => {
    const response = await app.inject({
      method: 'POST',
      url: '/calculate',
      payload: JSON.stringify({
        formula: '3*3',
      }),
      headers: {
        'content-type': 'application/json',
      },
    });

    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.body)).toEqual({ result: 9 });
  });
});
