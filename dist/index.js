"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const redis_1 = require("redis");
const create_server_1 = require("./create-server");
const cache_service_redis_1 = require("./services/cache/cache-service-redis");
const DAY_IN_SECONDS = 86400;
const thrower = (err) => () => {
    throw err;
};
process.on('unhandledRejection', (err) => {
    setTimeout(thrower(err), 0);
});
const main = () => __awaiter(void 0, void 0, void 0, function* () {
    const redisClient = redis_1.createClient({ url: process.env.REDIS_URL || 'redis://0.0.0.0:6379' });
    const cacheService = cache_service_redis_1.cacheServiceRedisFactory({ client: redisClient, ttl: DAY_IN_SECONDS });
    const server = yield create_server_1.createServer({ cacheService });
    return server.listen(9100, '0.0.0.0');
});
if (require.main) {
    main().catch((err) => {
        setTimeout(thrower(err), 0);
    });
}
