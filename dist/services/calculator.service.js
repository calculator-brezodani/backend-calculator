"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateResult = void 0;
const exact_math_1 = __importDefault(require("exact-math"));
exports.calculateResult = (value) => {
    const result = exact_math_1.default.formula(value);
    return result;
};
