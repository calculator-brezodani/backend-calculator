"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const create_server_1 = require("./create-server");
const start = () => __awaiter(void 0, void 0, void 0, function* () {
    create_server_1.createServer().listen(3000);
});
const thrower = (err) => () => {
    throw err;
};
process.on('unhandledRejection', (err) => {
    setTimeout(thrower(err), 0);
});
start().catch((err) => {
    setTimeout(thrower(err), 0);
});
