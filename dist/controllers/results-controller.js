"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postEvaluationFactory = void 0;
const boom_1 = __importDefault(require("boom"));
const calculator_service_1 = require("../services/calculator.service");
exports.postEvaluationFactory = (cache) => (formula) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const cachedResult = yield cache.get(formula);
        if (cachedResult) {
            return {
                result: cachedResult,
            };
        }
        const result = calculator_service_1.calculateResult(formula);
        yield cache.set(formula, result);
        return { result };
    }
    catch (err) {
        throw boom_1.default.boomify(err);
    }
});
