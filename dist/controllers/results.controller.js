"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLatestResult = exports.postEvaluation = void 0;
const boom_1 = __importDefault(require("boom"));
const result_1 = require("../models/result");
const calculator_service_1 = require("../services/calculator.service");
exports.postEvaluation = (value) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = calculator_service_1.calculateResult(value);
        const resultModel = new result_1.Result({ result });
        yield resultModel.save();
        return { result };
    }
    catch (err) {
        throw boom_1.default.boomify(err);
    }
});
exports.getLatestResult = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const latestResult = yield result_1.Result.find().limit(1).sort({ timeStamp: -1 });
        return latestResult[0];
    }
    catch (err) {
        throw boom_1.default.boomify(err);
    }
});
