"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.routesFactory = void 0;
const resultsController = __importStar(require("../controllers/results-controller"));
exports.routesFactory = ({ cacheService }) => (server, _, next) => {
    const postEvaluation = resultsController.postEvaluationFactory(cacheService);
    server.route({
        url: '/status',
        logLevel: 'warn',
        method: ['GET', 'HEAD'],
        handler: (_, reply) => __awaiter(void 0, void 0, void 0, function* () {
            return reply.send({ timeStamp: new Date() });
        }),
    });
    server.route({
        url: '/calculate',
        logLevel: 'warn',
        method: ['POST', 'HEAD'],
        schema: {
            body: {
                type: 'object',
                additionalProperties: false,
                required: ['formula'],
                properties: {
                    formula: {
                        type: 'string',
                    },
                },
            },
        },
        handler: (req) => postEvaluation(req.body.formula),
    });
    next();
};
