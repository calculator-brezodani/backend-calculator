"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const create_server_1 = require("../create-server");
const cache_service_stub_1 = require("../services/cache/cache-service-stub");
describe('route tests', () => {
    let app;
    beforeAll(() => __awaiter(void 0, void 0, void 0, function* () {
        app = yield create_server_1.createServer({
            cacheService: cache_service_stub_1.cacheServiceStub,
        });
        yield app.ready();
    }));
    it('should evaluate formula', () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield app.inject({
            method: 'POST',
            url: '/calculate',
            payload: JSON.stringify({
                formula: '3*3',
            }),
            headers: {
                'content-type': 'application/json',
            },
        });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.body)).toEqual({ result: 9 });
    }));
});
