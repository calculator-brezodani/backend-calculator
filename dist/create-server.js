"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
const fastify_1 = __importDefault(require("fastify"));
const routes_1 = require("./routes/routes");
const fastify_cors_1 = __importDefault(require("fastify-cors"));
exports.createServer = ({ cacheService }) => __awaiter(void 0, void 0, void 0, function* () {
    const server = fastify_1.default({ logger: true });
    yield server.register(fastify_cors_1.default, {
        origin: 'http://localhost:3000',
    });
    yield server.register(routes_1.routesFactory({ cacheService }));
    return server;
});
